﻿namespace DocumentManagementSystem
{
    partial class CreateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCreateUser = new System.Windows.Forms.Label();
            this.txtCreateSurname = new System.Windows.Forms.TextBox();
            this.txtCreateForename = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblForename = new System.Windows.Forms.Label();
            this.btnCreateUser = new System.Windows.Forms.Button();
            this.txtCreateUsername = new System.Windows.Forms.TextBox();
            this.lblCreateUsername = new System.Windows.Forms.Label();
            this.txtCreatePassword = new System.Windows.Forms.TextBox();
            this.lblCreatePassword = new System.Windows.Forms.Label();
            this.txtCreateEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnCancelCreate = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rBtnDistr = new System.Windows.Forms.RadioButton();
            this.rBtnAuthor = new System.Windows.Forms.RadioButton();
            this.rBtnAdmin = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCreateUser
            // 
            this.lblCreateUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateUser.Location = new System.Drawing.Point(333, 19);
            this.lblCreateUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCreateUser.Name = "lblCreateUser";
            this.lblCreateUser.Size = new System.Drawing.Size(465, 92);
            this.lblCreateUser.TabIndex = 13;
            this.lblCreateUser.Text = "Create User";
            // 
            // txtCreateSurname
            // 
            this.txtCreateSurname.Location = new System.Drawing.Point(348, 325);
            this.txtCreateSurname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCreateSurname.Name = "txtCreateSurname";
            this.txtCreateSurname.Size = new System.Drawing.Size(355, 26);
            this.txtCreateSurname.TabIndex = 11;
            // 
            // txtCreateForename
            // 
            this.txtCreateForename.Location = new System.Drawing.Point(348, 226);
            this.txtCreateForename.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCreateForename.Name = "txtCreateForename";
            this.txtCreateForename.Size = new System.Drawing.Size(355, 26);
            this.txtCreateForename.TabIndex = 10;
            // 
            // lblSurname
            // 
            this.lblSurname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Location = new System.Drawing.Point(348, 278);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(202, 42);
            this.lblSurname.TabIndex = 9;
            this.lblSurname.Text = "Surname:";
            // 
            // lblForename
            // 
            this.lblForename.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblForename.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForename.Location = new System.Drawing.Point(348, 179);
            this.lblForename.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForename.Name = "lblForename";
            this.lblForename.Size = new System.Drawing.Size(202, 42);
            this.lblForename.TabIndex = 8;
            this.lblForename.Text = "Forename:";
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateUser.Location = new System.Drawing.Point(796, 425);
            this.btnCreateUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.Size = new System.Drawing.Size(332, 101);
            this.btnCreateUser.TabIndex = 7;
            this.btnCreateUser.Text = "Submit";
            this.btnCreateUser.UseVisualStyleBackColor = false;
            this.btnCreateUser.Click += new System.EventHandler(this.btnCreateUser_Click);
            // 
            // txtCreateUsername
            // 
            this.txtCreateUsername.Location = new System.Drawing.Point(348, 425);
            this.txtCreateUsername.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCreateUsername.Name = "txtCreateUsername";
            this.txtCreateUsername.Size = new System.Drawing.Size(355, 26);
            this.txtCreateUsername.TabIndex = 16;
            // 
            // lblCreateUsername
            // 
            this.lblCreateUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreateUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateUsername.Location = new System.Drawing.Point(348, 378);
            this.lblCreateUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCreateUsername.Name = "lblCreateUsername";
            this.lblCreateUsername.Size = new System.Drawing.Size(202, 42);
            this.lblCreateUsername.TabIndex = 14;
            this.lblCreateUsername.Text = "Username:";
            // 
            // txtCreatePassword
            // 
            this.txtCreatePassword.Location = new System.Drawing.Point(348, 532);
            this.txtCreatePassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCreatePassword.MaxLength = 16;
            this.txtCreatePassword.Name = "txtCreatePassword";
            this.txtCreatePassword.Size = new System.Drawing.Size(355, 26);
            this.txtCreatePassword.TabIndex = 20;
            // 
            // lblCreatePassword
            // 
            this.lblCreatePassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreatePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatePassword.Location = new System.Drawing.Point(348, 485);
            this.lblCreatePassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCreatePassword.Name = "lblCreatePassword";
            this.lblCreatePassword.Size = new System.Drawing.Size(202, 42);
            this.lblCreatePassword.TabIndex = 19;
            this.lblCreatePassword.Text = "Password:";
            // 
            // txtCreateEmail
            // 
            this.txtCreateEmail.Location = new System.Drawing.Point(348, 634);
            this.txtCreateEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCreateEmail.Name = "txtCreateEmail";
            this.txtCreateEmail.Size = new System.Drawing.Size(355, 26);
            this.txtCreateEmail.TabIndex = 22;
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(348, 586);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(202, 42);
            this.lblEmail.TabIndex = 21;
            this.lblEmail.Text = "Email:";
            // 
            // btnCancelCreate
            // 
            this.btnCancelCreate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCancelCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelCreate.Location = new System.Drawing.Point(796, 546);
            this.btnCancelCreate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancelCreate.Name = "btnCancelCreate";
            this.btnCancelCreate.Size = new System.Drawing.Size(332, 101);
            this.btnCancelCreate.TabIndex = 23;
            this.btnCancelCreate.Text = "Cancel";
            this.btnCancelCreate.UseVisualStyleBackColor = false;
            this.btnCancelCreate.Click += new System.EventHandler(this.btnCancelCreate_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(18, 19);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(238, 261);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rBtnDistr);
            this.groupBox1.Controls.Add(this.rBtnAuthor);
            this.groupBox1.Controls.Add(this.rBtnAdmin);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.Location = new System.Drawing.Point(796, 179);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(300, 204);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Role";
            // 
            // rBtnDistr
            // 
            this.rBtnDistr.AutoSize = true;
            this.rBtnDistr.Location = new System.Drawing.Point(9, 148);
            this.rBtnDistr.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rBtnDistr.Name = "rBtnDistr";
            this.rBtnDistr.Size = new System.Drawing.Size(159, 33);
            this.rBtnDistr.TabIndex = 2;
            this.rBtnDistr.TabStop = true;
            this.rBtnDistr.Text = "Distributee";
            this.rBtnDistr.UseVisualStyleBackColor = true;
            // 
            // rBtnAuthor
            // 
            this.rBtnAuthor.AutoSize = true;
            this.rBtnAuthor.Location = new System.Drawing.Point(9, 98);
            this.rBtnAuthor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rBtnAuthor.Name = "rBtnAuthor";
            this.rBtnAuthor.Size = new System.Drawing.Size(112, 33);
            this.rBtnAuthor.TabIndex = 1;
            this.rBtnAuthor.TabStop = true;
            this.rBtnAuthor.Text = "Author";
            this.rBtnAuthor.UseVisualStyleBackColor = true;
            // 
            // rBtnAdmin
            // 
            this.rBtnAdmin.AutoSize = true;
            this.rBtnAdmin.Location = new System.Drawing.Point(9, 48);
            this.rBtnAdmin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rBtnAdmin.Name = "rBtnAdmin";
            this.rBtnAdmin.Size = new System.Drawing.Size(188, 33);
            this.rBtnAdmin.TabIndex = 0;
            this.rBtnAdmin.TabStop = true;
            this.rBtnAdmin.Text = "Administrator";
            this.rBtnAdmin.UseVisualStyleBackColor = true;
            // 
            // CreateUser
            // 
            this.AcceptButton = this.btnCreateUser;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(1389, 825);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancelCreate);
            this.Controls.Add(this.txtCreateEmail);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtCreatePassword);
            this.Controls.Add(this.lblCreatePassword);
            this.Controls.Add(this.txtCreateUsername);
            this.Controls.Add(this.lblCreateUsername);
            this.Controls.Add(this.lblCreateUser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtCreateSurname);
            this.Controls.Add(this.txtCreateForename);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.lblForename);
            this.Controls.Add(this.btnCreateUser);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CreateUser";
            this.Text = "CreateUser";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCreateUser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtCreateSurname;
        private System.Windows.Forms.TextBox txtCreateForename;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblForename;
        private System.Windows.Forms.Button btnCreateUser;
        private System.Windows.Forms.TextBox txtCreateUsername;
        private System.Windows.Forms.Label lblCreateUsername;
        private System.Windows.Forms.TextBox txtCreatePassword;
        private System.Windows.Forms.Label lblCreatePassword;
        private System.Windows.Forms.TextBox txtCreateEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnCancelCreate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rBtnDistr;
        private System.Windows.Forms.RadioButton rBtnAuthor;
        private System.Windows.Forms.RadioButton rBtnAdmin;
    }
}