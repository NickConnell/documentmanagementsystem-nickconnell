USERS
=======================================================================================================================
/* NOTE
* ------------
* The length of the value of password has to be between 8-16. That is the string has to have between 8-16 letters.
* Value of role is either 0, 1 or 2.
* 0 stands for user being a author, 1 for distributee and 2 for administrator.
* Value of status can either be 0 or 1. 
* 0 Stands for user being active, 1 stands for him being archived.
*/

CREATE TABLE Users(
	userID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
	username VARCHAR(20) NOT NULL UNIQUE, 
	password VARCHAR, 
	forename VARCHAR(255), 
	surname VARCHAR(255), 
	role INTEGER(1),						
	status INTEGER(1),						
	regDate DATETIME DEFAULT CURRENT_TIMESTAMP,			
	CHECK(length(password) >= 8 AND length(password) <= 16),	
	CHECK(role <= 2 AND role >= 0)
	CHECK(status <=1 AND status >= 0)
);

INSERT INTO Users(username, password, forename, surname, role, status) VALUES("atlijohann", "asdf1234", "Atli", "Johannsson", 0, 0);
INSERT INTO Users(username, password, forename, surname, role, status) VALUES("nickconnel", "asdf1234", "Nick", "Connel", 1, 0);
INSERT INTO Users(username, password, forename, surname, role, status) VALUES("jamieglend", "asdf1234", "Jamie", "Glendinning", 2, 0);
INSERT INTO Users(username, password, forename, surname, role, status) VALUES("frazerjohn", "asdf1234", "Frazer", "Johhnston", 0, 1);
INSERT INTO Users(username, password, forename, surname, role, status) VALUES("chrissharp", "asdf1234", "Christopher", "Sharp", 1, 1);
INSERT INTO Users(username, password, forename, surname, role, status) VALUES("johndoe", "asdf1234", "John", "Doe", 2, 1);




userID  username    password    forename         surname          role  status  regDate
------  ----------  ----------  ---------------  ---------------  ----  ------  -------------------
1       atlijohann  asdf1234    Atli             Johannsson       0     0       2017-02-26 19:29:16
2       nickconnel  asdf1234    Nick             Connel           1     0       2017-02-26 19:29:16
3       jamieglend  asdf1234    Jamie            Glendinning      2     0       2017-02-26 19:29:16
4       frazerjohn  asdf1234    Frazer           Johhnston        0     1       2017-02-26 19:29:16
5       chrissharp  asdf1234    Christopher      Sharp            1     1       2017-02-26 19:29:16
6       johndoe     asdf1234    John             Doe              2     1       2017-02-26 19:29:18





DOCUMENTS
=======================================================================================================================
/* NOTE
* ------------
* Value of status can either be 0 or 1. 
* 0 Stands for document being active, 1 stands for it being a draft.
* actDate is the date when document is activated.
* The value of status can not be 0 and actDate NULL and vice versa, , that is a document 
* can't be a draft and have a activation date, or be active and not have a activation date.
*/

CREATE TABLE Documents(
	docID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	title VARCHAR(255),
	revNum INTEGER(3),
	author INTEGER(3),
	status INTEGER(1),								
	desc VARCHAR(255),
	createDate DATETIME DEFAULT CURRENT_TIMESTAMP,					
	actDate Date,
	FOREIGN KEY(author) REFERENCES Users(userID),
	CHECK(status <= 1 AND status >= 0),						
	CHECK((status = 1 AND actDate IS NULL) OR (status = 0 AND actDate IS NOT NULL))	
);

INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Atlis Active Document", 1, 1, 0, "Document that is active and created by Atli.", "2017-26-02");
INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Atlis Draft Document", 1, 1, 1, "Document that is a draft and created by Atli.", NULL);

INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Nicks Active Document", 1, 2, 0, "Document that is active and created by Nick.", "2017-26-02");
INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Nicks Draft Document", 1, 2, 1, "Document that is a draft and created by Nick.", NULL);

INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Jamies Active Document", 1, 3, 0, "Document that is active and created by Jamie.", "2017-26-02");
INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Jamies Draft Document", 1, 3, 1, "Document that is a draft and created by Jamie.", NULL);

INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Frazers Active Document", 1, 4, 0, "Document that is active and created by Frazer.", "2017-26-02");
INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Frazers Draft Document", 1, 4, 1, "Document that is a draft and created by Frazer.", NULL);

INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Chris' Active Document", 1, 5, 0, "Document that is active and created by Chris.", "2017-26-02");
INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Chris' Draft Document", 1, 5, 1, "Document that is a draft and created by Chris.", NULL);

INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Johns Active Document", 1, 6, 0, "Document that is active and created by John.", "2017-26-02");
INSERT INTO Documents(title, revNum, author, status, desc, actDate) VALUES("Johns Document", 1, 6, 1, "Document that is a draft and created by John.", NULL);




docID  title                    revNum  author  status  desc                                             createDate           actDate
-----  -----------------------  ------  ------  ------  -----------------------------------------------  -------------------  ----------
1      Atlis Active Document    1       1       0       Document that is active and created by Atli.     2017-02-26 19:38:04  2017-26-02
2      Atlis Draft Document     1       1       1       Document that is a draft and created by Atli.    2017-02-26 19:38:04
3      Nicks Active Document    1       2       0       Document that is active and created by Nick.     2017-02-26 19:38:04  2017-26-02
4      Nicks Draft Document     1       2       1       Document that is a draft and created by Nick.    2017-02-26 19:38:04
5      Jamies Active Document   1       3       0       Document that is active and created by Jamie.    2017-02-26 19:38:04  2017-26-02
6      Jamies Draft Document    1       3       1       Document that is a draft and created by Jamie.   2017-02-26 19:38:04
7      Frazers Active Document  1       4       0       Document that is active and created by Frazer.   2017-02-26 19:38:04  2017-26-02
8      Frazers Draft Document   1       4       1       Document that is a draft and created by Frazer.  2017-02-26 19:38:04
9      Chris' Active Document   1       5       0       Document that is active and created by Chris.    2017-02-26 19:38:04  2017-26-02
10     Chris' Draft Document    1       5       1       Document that is a draft and created by Chris.   2017-02-26 19:38:04
11     Johns Active Document    1       6       0       Document that is active and created by John.     2017-02-26 19:38:04  2017-26-02
12     Johns Document           1       6       1       Document that is a draft and created by John.    2017-02-26 19:38:05





DISTRIBUTIONS
=======================================================================================================================
CREATE TABLE Distributions(
	docID INTEGER(3),
	author INTEGER(3),
	createDate Date,
	distID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	FOREIGN KEY(docID) REFERENCES Documents(docID),
	FOREIGN KEY(author) REFERENCES Users(userID)
);

INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(1, 1, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(2, 1, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(3, 2, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(4, 2, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(5, 3, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(6, 3, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(7, 4, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(8, 4, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(9, 5, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(10, 5, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(11, 6, "2017-02-26 19:38:04");
INSERT INTO DISTRIBUTIONS(docID, author, createDate) VALUES(12, 6, "2017-02-26 19:38:05");




docID  author  createDate           distID
-----  ------  -------------------  -------
1      1       2017-02-26 19:38:04  1
2      1       2017-02-26 19:38:04  2
3      2       2017-02-26 19:38:04  3
4      2       2017-02-26 19:38:04  4
5      3       2017-02-26 19:38:04  5
6      3       2017-02-26 19:38:04  6
7      4       2017-02-26 19:38:04  7
8      4       2017-02-26 19:38:04  8
9      5       2017-02-26 19:38:04  9
10     5       2017-02-26 19:38:04  10
11     6       2017-02-26 19:38:04  11
12     6       2017-02-26 19:38:05  12