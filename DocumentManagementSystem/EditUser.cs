﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentManagementSystem
{
    public partial class EditUser : Form
    {
        private string username;

        public EditUser()
        {
            InitializeComponent();
        }

        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            // Checks if something was edited at all.
            if (string.IsNullOrWhiteSpace(txtCreateUsername.Text) && string.IsNullOrWhiteSpace(txtCreatePassword.Text) &&
                string.IsNullOrWhiteSpace(txtCreateForename.Text) && string.IsNullOrWhiteSpace(txtCreateSurname.Text) &&
                (!rBtnAdmin.Checked && !rBtnAuthor.Checked && !rBtnDistr.Checked))
            {
                MessageBox.Show("All fields are blank.");
            }
            else
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to edit user?", "Sure", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    string updateDetails = "Following details have been updated:";

                    if (!string.IsNullOrWhiteSpace(txtCreatePassword.Text))
                    {
                        if (txtCreatePassword.Text.Length < 8)
                        {
                            MessageBox.Show("Password should be at least 8 characters long.");
                            return;
                        }
                        else
                        {
                            sqlWorkBench.updateUserDetails(1, txtCreatePassword.Text, username);
                            updateDetails += "\nPassword";
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(txtCreateUsername.Text))
                    {
                        sqlWorkBench.updateUserDetails(0, txtCreateUsername.Text, username);
                        username = txtCreateUsername.Text;
                        updateDetails += "\nUsername";
                    }
                    if (!string.IsNullOrWhiteSpace(txtCreateForename.Text))
                    {
                        sqlWorkBench.updateUserDetails(2, txtCreateForename.Text, username);
                        updateDetails += "\nForename";
                    }
                    if (!string.IsNullOrWhiteSpace(txtCreateSurname.Text))
                    {
                        sqlWorkBench.updateUserDetails(3, txtCreateSurname.Text, username);
                        updateDetails += "\nSurname";
                    }

                    if (rBtnAuthor.Checked)
                    {
                        sqlWorkBench.updateUserDetails(4, "0", username);
                        updateDetails += "\nRole";
                    }
                    else if (rBtnDistr.Checked)
                    {
                        sqlWorkBench.updateUserDetails(4, "1", username);
                        updateDetails += "\nRole";
                    }
                    else if (rBtnAdmin.Checked)
                    {
                        sqlWorkBench.updateUserDetails(4, "2", username);
                        updateDetails += "\nRole";
                    }
                    MessageBox.Show(updateDetails);

                    txtCreateForename.Text = string.Empty;
                    txtCreateSurname.Text = string.Empty;
                    txtCreateUsername.Text = string.Empty;
                    txtCreatePassword.Text = string.Empty;

                    rBtnAdmin.Checked = false;
                    rBtnAuthor.Checked = false;
                    rBtnDistr.Checked = false;
                }
            }
        }

        private void btnValUser_Click(object sender, EventArgs e)
        {
            if (sqlWorkBench.userExists(txtUsername.Text))
            {
                username = txtUsername.Text;
                txtCreateUsername.Text = username;
                panel1.Hide();
                panel2.Show();
            }
            else
            {
                MessageBox.Show("User does not exist.");
                txtUsername.Text = string.Empty;
            }
        }

        private void btnCancelCreate_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete user?", "Sure", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                sqlWorkBench.deleteUser(username);
                Close();
            }
        }
    }
}
